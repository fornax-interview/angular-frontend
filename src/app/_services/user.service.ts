import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@model/user';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private userList: User[] = [{ id: 1, name: 'Pisti', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() }];

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]> {
        return of(this.userList);
    }
}
